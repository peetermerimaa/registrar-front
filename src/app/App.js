import React from "react";
import "./styles.css";
import logo from "./logo.svg";
import Container from "../component/container/Container";
import { observer } from "mobx-react";

@observer
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          AjaLeht
        </header>
        <div className="App-container">
          <Container />
        </div>
      </div>
    );
  }
}
export default App;
