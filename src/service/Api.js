import { baseURL } from "./constants";

class Api {
  static getCompetitions() {
    return fetch(`${baseURL}/competition/get`).then(response =>
      response.json()
    );
  }

  static getCompetition(id) {
    return fetch(`${baseURL}/competition/get/${id}`).then(response =>
      response.json()
    );
  }

  static getCompetitors(idCompetition) {
    return fetch(
      `${baseURL}/participant/competitors/${idCompetition}`
    ).then(response => response.json());
  }

  static getRaceResults(idCompetition) {
    return fetch(
      `${baseURL}/participant/results/${idCompetition}`
    ).then(response => response.json());
  }

  static addParticipant(params, idCompetition) {
    return fetch(`${baseURL}/participant/${idCompetition}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: params.name,
        surename: params.surename,
        birthYear: params.birthYear,
        sex: params.sex,
        club: params.club,
        email: params.email
      })
    }).then(response => response.json());
  }
}
export default Api;
