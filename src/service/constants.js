export const menuParticipant = [
  "Avaleht",
  "Algavad võistlused",
  "Toimunud võistlused"
];
export const menuAdmin = ["Home", "Services", "About"];
export const token =
  "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiJ9.JUGDZs3Eyf6G08Y9JsGHxLBscBWsiJVeKfsH1302nCYO1xiwASrsV_I5TcAL8Dc06K0eOTXncVFmWHCyWhw1fA";
export const baseURL =
  process.env.NODE_ENV === "development"
    ? "http://localhost:8080/api/v1"
    : "http://167.71.13.163:8080/api/v1";
