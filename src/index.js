import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { createBrowserHistory } from "history";
import { Provider } from "mobx-react";
import { RouterStore, syncHistoryWithStore } from "mobx-react-router";
import { Router } from "react-router";
import App from "./app/App";
import * as serviceWorker from "./serviceWorker";
import NavigationStore from "./component/navigation/NavigationStore";

const browserHistory = createBrowserHistory();
const routerStore = new RouterStore();
const navigationStore = new NavigationStore();

const stores = {
  routing: routerStore,
  navigation: navigationStore
};

const history = syncHistoryWithStore(browserHistory, routerStore);

ReactDOM.render(
  <Provider {...stores}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept();
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
