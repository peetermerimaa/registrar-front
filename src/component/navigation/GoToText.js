import React from "react";

class GoToText extends React.Component {
  render() {
    const isAdmin = this.props.target === "admin";
    const goTo = "LEHELE";
    const toAdmin = "ADMINI";
    const toParticipant = "KASUTAJA";
    const target = isAdmin ? toAdmin : toParticipant;
    const transformStyle = isAdmin
      ? { transform: "rotateZ(10deg)", paddingLeft: "20px" }
      : { transform: "rotateZ(-10deg)" };

    return (
      <div className="goToText" style={transformStyle}>
        <div>{target}</div>
        <div>{goTo}</div>
      </div>
    );
  }
}

export default GoToText;
