import React from "react";
import { inject, observer } from "mobx-react";
import { computed } from "mobx";

@inject("routing", "navigation")
@observer
class ComponentMenu extends React.Component {
  clicked = item => {
    const { target, routing } = this.props;
    routing.push(`${target}/${item}`);
  };

  @computed
  get focusedPath() {
    const { routing, navigation, target } = this.props;

    return navigation.focused(routing.location, target);
  }

  render() {
    var self = this;
    const { items } = this.props;
    const focused = this.focusedPath;

    return (
      <div>
        <ul className="flexContainer">
          {items.map(function(item, index) {
            var style = "btn-group";
            if (focused === index) {
              style = "btn-group focused";
            }
            return (
              <li
                key={index.toString()}
                className={style}
                onClick={self.clicked.bind(self, item)}
              >
                <button>{item}</button>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}

export default ComponentMenu;
