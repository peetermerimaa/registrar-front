import { menuAdmin, menuParticipant } from "../../service/constants";

export default class NavigationStore {
  static SELF = "/";
  static ADMIN = "/admin";
  static PARTICIPANT = "/kasutaja";

  targetParticipant = [
    `${NavigationStore.PARTICIPANT}/${menuParticipant[0]}`,
    `${NavigationStore.PARTICIPANT}/${menuParticipant[1]}`,
    `${NavigationStore.PARTICIPANT}/${menuParticipant[2]}`,
    `${NavigationStore.PARTICIPANT}/${menuParticipant[3]}`
  ];
  targetAdmin = [
    `${NavigationStore.ADMIN}/${menuAdmin[0]}`,
    `${NavigationStore.ADMIN}/${menuAdmin[1]}`,
    `${NavigationStore.ADMIN}/${menuAdmin[2]}`,
    `${NavigationStore.ADMIN}/${menuAdmin[3]}`
  ];

  focused(location, target) {
    if (
      location.pathname === NavigationStore.SELF ||
      location.pathname === NavigationStore.ADMIN ||
      location.pathname === NavigationStore.PARTICIPANT
    ) {
      return 0;
    }

    return target === NavigationStore.PARTICIPANT
      ? this.targetParticipant.indexOf(location.pathname)
      : this.targetAdmin.indexOf(location.pathname);
  }
}
