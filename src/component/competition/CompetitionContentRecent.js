import React from "react";
import { observer } from "mobx-react";
import RaceResult from "../raceResult/RaceResult";

@observer
class CompetitionContentRecent extends React.Component {
  componentDidMount() {
    const { item, store } = this.props;
    store.clear();
    store.getRaceResults(item.id);
  }

  renderRaceResultList(raceResults) {
    return raceResults.map((result, index) => (
      <RaceResult key={`${index}-${result.name}`} item={result} />
    ));
  }

  render() {
    const { store } = this.props;

    return (
      <div className="itemContent">
        <div>
          {store.raceResults && this.renderRaceResultList(store.raceResults)}
        </div>
      </div>
    );
  }
}

export default CompetitionContentRecent;
