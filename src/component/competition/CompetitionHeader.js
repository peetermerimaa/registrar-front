import React from "react";
import { observer } from "mobx-react";
import arrow from "./arrow-down.svg";
import Competition from "./Competition";

@observer
class CompetitionHeader extends React.Component {
  arrowClicked = () => {
    const { store } = this.props;
    store.setIsRegistrationStarted(false);
    store.setAccordionsClosed();
  };

  render() {
    const { type, item, isToggled } = this.props;
    const competitionInfo = `${item.date} / ${item.place} / ${item.name}`;
    const resultsTilte = "Tulemused";
    const headerTitle =
      type === Competition.RECENT
        ? `${competitionInfo} / ${resultsTilte}`
        : competitionInfo;
    const arrowStyle = isToggled ? "arrowUp right" : "arrowDown right";

    return (
      <div className="itemHeader">
        <span className="left">{headerTitle}</span>
        <img
          src={arrow}
          alt=""
          className={arrowStyle}
          onClick={isToggled ? this.arrowClicked : undefined}
        />
      </div>
    );
  }
}

export default CompetitionHeader;
