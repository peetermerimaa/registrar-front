import React from "react";
import { observer } from "mobx-react";
import RegisterForm from "../registerForm/RegisterForm";
import RegisterFormButton from "../registerForm/RegisterFormButton";

@observer
class CompetitionContentUpcoming extends React.Component {
  splitedDescription = () => {
    const { description } = this.props.item;

    return description.split("\\n").map((item, key) => {
      return (
        <span key={key}>
          {item}
          <br />
        </span>
      );
    });
  };

  render() {
    const { item, store } = this.props;

    return (
      <div className="itemContent">
        <div className="flex-column padding-right">
          <RegisterFormButton store={store} />
        </div>
        {store.isRegistrationStarted ? (
          <RegisterForm competitionId={item.id} />
        ) : (
          <div>{this.splitedDescription()}</div>
        )}
      </div>
    );
  }
}

export default CompetitionContentUpcoming;
