import React from "react";
import { observer } from "mobx-react";
import CompetitionHeader from "./CompetitionHeader";
import CompetitionContentUpcoming from "./CompetitionContentUpcoming";
import CompetitionContentRecent from "./CompetitionContentRecent";

@observer
class Competition extends React.Component {
  clicked = () => {
    const { item, store } = this.props;
    store.setIsRegistrationStarted(false);
    store.setAccordionOpened(item.id);
  };

  renderContent() {
    const { type, item, store } = this.props;

    return type === Competition.UPCOMING ? (
      <CompetitionContentUpcoming item={item} store={store} />
    ) : (
      <CompetitionContentRecent item={item} store={store} />
    );
  }

  render() {
    const { type, item, store } = this.props;
    const isToggled = store.isAccordionActive(item.id);
    const componentListStyle = isToggled
      ? "componentList"
      : "componentList cursor-pointer";

    return (
      <div
        className={componentListStyle}
        onClick={isToggled ? undefined : this.clicked}
      >
        <CompetitionHeader
          type={type}
          item={item}
          store={store}
          isToggled={isToggled}
        />
        {isToggled && this.renderContent()}
      </div>
    );
  }
}

export default Competition;

Competition.UPCOMING = "UPCOMING";
Competition.RECENT = "RECENT";
