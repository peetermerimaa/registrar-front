import React from "react";
import { Switch, Route } from "react-router-dom";
import Slider from "./Slider";
import NavigationViewStore from "../navigation/NavigationStore";

class SlideOut extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      childPosition: Slider.CENTER,
      curChild: props.children,
      curUniqId: props.uniqId,
      prevChild: null,
      prevUniqId: null,
      animationCallback: null
    };
  }

  componentDidUpdate(prevProps) {
    const prevUniqId = prevProps.uniqKey;
    const uniqId = this.props.uniqKey;
    const position = this.isAnimatedSwitch(uniqId)
      ? Slider.TO_LEFT
      : Slider.CENTER;

    if (prevUniqId !== uniqId) {
      this.setState({
        childPosition: position,
        curChild: this.props.children,
        curUniqId: uniqId,
        prevChild: prevProps.children,
        prevUniqId,
        animationCallback: this.swapChildren
      });
    }
  }

  isAnimatedSwitch(uniqId) {
    return (
      uniqId === NavigationViewStore.ADMIN ||
      uniqId === NavigationViewStore.PARTICIPANT
    );
  }

  swapChildren = () => {
    const uniqId = this.props.uniqKey;
    const position = this.isAnimatedSwitch(uniqId)
      ? Slider.FROM_RIGHT
      : Slider.CENTER;

    this.setState({
      childPosition: position,
      prevChild: null,
      prevUniqId: null,
      animationCallback: null
    });
  };

  render() {
    return (
      <Slider
        position={this.state.childPosition}
        animationCallback={this.state.animationCallback}
      >
        {this.state.curChild}
      </Slider>
    );
  }
}

const animateSwitch = (CustomSwitch, AnimatorComponent) => ({
  updateStep,
  children
}) => (
  <Route
    render={({ location }) => (
      <AnimatorComponent uniqKey={location.pathname} updateStep={updateStep}>
        <CustomSwitch location={location}>{children}</CustomSwitch>
      </AnimatorComponent>
    )}
  />
);

const SwitchWithSlide = animateSwitch(Switch, SlideOut);

export default SwitchWithSlide;
