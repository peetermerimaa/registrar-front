import React from "react";
import { observer } from "mobx-react";
import { Switch, Link, Route } from "react-router-dom";
import SwitchWithSlide from "./SwitchWithSlide";
import Admin from "../admin/Admin";
import ComponentMenu from "../navigation/ComponentMenu";
import Participant from "../participant/Participant";
import arrow from "./arrow.png";
import { menuAdmin, menuParticipant } from "../../service/constants";
import GoToText from "../navigation/GoToText";
import NavigationStore from "../navigation/NavigationStore";

@observer
class Container extends React.Component {
  animate = true;

  render() {
    const SwitchComponent = this.animate ? SwitchWithSlide : Switch;
    const admin = NavigationStore.ADMIN;
    const kasutaja = NavigationStore.PARTICIPANT;

    return (
      <React.Fragment>
        <div className="mainContainer">
          <SwitchComponent>
            <Route
              path={admin}
              render={() => (
                <div>
                  <div className="flexContainer">
                    <Link to={kasutaja} className="flexContainer">
                      <img src={arrow} alt="" className="arrowl"></img>
                      <GoToText target="client" />
                    </Link>
                    <ComponentMenu items={menuAdmin} target={admin} />
                  </div>
                  <Admin />
                </div>
              )}
            />
            <Route
              path="/"
              render={() => (
                <div>
                  <div className="flexContainer">
                    <ComponentMenu items={menuParticipant} target={kasutaja} />
                    <Link to={admin} className="flexContainer">
                      <GoToText target="admin" />
                      <img src={arrow} alt="" className="arrow "></img>
                    </Link>
                  </div>
                  <Participant />
                </div>
              )}
            />
          </SwitchComponent>
        </div>
      </React.Fragment>
    );
  }
}

export default Container;
