import React from "react";

class NotFound extends React.Component {
  render() {
    return (
      <div style={{ display: "flex", minHeight: "300px" }}>
        <h1 style={{ margin: "auto" }}>
          404
          <br />
          NOT FOUND
        </h1>
      </div>
    );
  }
}

export default NotFound;
