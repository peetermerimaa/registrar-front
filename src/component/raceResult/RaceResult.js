import React from "react";
import { observer } from "mobx-react";

@observer
class RaceResult extends React.Component {
  render() {
    const { item } = this.props;

    return (
      <div className="result-container">
        <ul>
          <li style={{ width: 50 }}> {item.number}</li>
          <li style={{ width: 150 }}> {`${item.name} ${item.surename}`}</li>
          <li style={{ width: 50 }}> {item.birthYear}</li>
          <li style={{ width: 20 }}> {item.sex}</li>
          <li style={{ width: 150 }}> {item.club}</li>
          <li style={{ width: 100 }}> {item.raceTime}</li>
        </ul>
      </div>
    );
  }
}

export default RaceResult;
