import React from "react";
import { observer } from "mobx-react";
import RegisterFormStore from "./RegisterFormStore";

@observer
class RegisterForm extends React.Component {
  store = new RegisterFormStore();

  componentDidMount() {
    this.store.setCompetitionId(this.props.competitionId);
  }

  renderOptionList(competitions) {
    return competitions.map((competition, index) => (
      <option value={competition.id} key={`${index}-${competition.name}`}>
        {competition.name}
      </option>
    ));
  }

  render() {
    const { store } = this;
    const chooseSex = <div style={{ paddingRight: 5 }}>Sugu:</div>;

    return (
      <form onSubmit={store.handleSubmit} className="flex-column">
        <div className="flex-row">
          <div className="flex-column">
            <label>
              <input
                name="name"
                type="text"
                value={store.name}
                placeholder="Eesnimi:"
                onChange={store.setName}
              />
            </label>
            <label>
              <input
                name="surename"
                type="text"
                value={store.surename}
                placeholder="Perekonnanimi:"
                onChange={store.setSurename}
              />
            </label>
            <label>
              <input
                name="surename"
                type="text"
                value={store.birthYear}
                placeholder="Sünniaasta:"
                onChange={store.setBirthYear}
              />
            </label>
          </div>
          <div className="flex-column">
            <label>
              <input
                name="surename"
                type="text"
                value={store.club}
                placeholder="Klubi:"
                onChange={store.setClub}
              />
            </label>
            <label>
              <input
                name="surename"
                type="text"
                value={store.email}
                placeholder="E-post:"
                onChange={store.setEmail}
              />
            </label>
            <label className="flex-row">
              {chooseSex}
              <select value={store.sex} onChange={store.setSex}>
                {!store.sex && <option value={undefined}></option>}
                <option value={"M"}>M</option>
                <option value={"N"}>N</option>
              </select>
            </label>
          </div>
        </div>
        <label>
          <input
            type="submit"
            value="Submit"
            disabled={!store.canBeSubmitted}
          />
        </label>
      </form>
    );
  }
}
export default RegisterForm;
