import React from "react";
import { observer } from "mobx-react";

@observer
class RegisterFormButton extends React.Component {
  handleClick = () => {
    const { store } = this.props;
    store.setIsRegistrationStarted(!store.isRegistrationStarted);
  };

  buttonStyle = () => ({
    backgroundColor: "#61dafb",
    fontSize: "17px",
    color: "black",
    height: "30px",
    borderRadius: "6px",
    borderStyle: "solid",
    borderWidth: "1px",
    borderColor: "black",
    marginTop: "10px"
  });

  render() {
    const cancel = "Loobun";
    const register = "Registreerin";
    const { store } = this.props;

    return (
      <button onClick={this.handleClick} style={this.buttonStyle()}>
        {store.isRegistrationStarted ? cancel : register}
      </button>
    );
  }
}
export default RegisterFormButton;
