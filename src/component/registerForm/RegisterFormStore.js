import Api from "../../service/Api";
import { observable, action, computed } from "mobx";

class RegisterFormStore {
  @observable competitionId;
  @observable name = "";
  @observable surename = "";
  @observable birthYear = "";
  @observable sex = "";
  @observable club = "";
  @observable email = "";
  @observable canShow = false;

  handleSubmit = async event => {
    event.preventDefault();
    if (!this.canBeSubmitted) {
      return;
    }
    const params = {
      name: this.name,
      surename: this.surename,
      birthYear: this.birthYear,
      sex: this.sex,
      club: this.club,
      email: this.email
    };

    return await Api.addParticipant(params, this.competitionId);
  };

  @computed
  get canBeSubmitted() {
    return (
      this.nameFieldsAreValid &&
      this.birthYearIsValid &&
      this.clubIsValid &&
      !!this.sex &&
      this.emailIsValid
    );
  }

  @computed
  get nameFieldsAreValid() {
    return (
      this.name.length > 0 &&
      this.name.length < 40 &&
      this.surename.length > 0 &&
      this.surename.length < 40
    );
  }

  @computed
  get birthYearIsValid() {
    return this.birthYear.length === 4 && this.birthYear.match(/^[0-9]+$/);
  }

  @computed
  get clubIsValid() {
    return this.club.length > 0 && this.club.length < 40;
  }

  @computed
  get emailIsValid() {
    return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(this.email);
  }

  @action
  setCompetitionId = competitionId => {
    this.competitionId = competitionId;
  };

  @action
  setName = event => {
    this.name = event.target.value;
  };
  @action
  setSurename = event => {
    this.surename = event.target.value;
  };
  @action
  setSex = event => {
    this.sex = event.target.value;
  };
  @action
  setClub = event => {
    this.club = event.target.value;
  };
  @action
  setEmail = event => {
    this.email = event.target.value;
  };
  @action
  setBirthYear = event => {
    this.birthYear = event.target.value;
  };

  @action
  setCompetitions(competitions) {
    this.competitions = competitions;
  }
}
export default RegisterFormStore;
