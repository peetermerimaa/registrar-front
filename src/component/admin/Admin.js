import React from "react";
import { inject } from "mobx-react";
import NavigationStore from "../navigation/NavigationStore";
import NotFound from "../container/NotFound";

@inject("routing", "navigation")
class Admin extends React.Component {
  render() {
    const { routing, navigation } = this.props;
    const focused = navigation.focused(routing.location, NavigationStore.ADMIN);

    return (
      <div className="box">
        {focused === 0 && focused}
        {focused === 1 && focused}
        {focused === 2 && focused}
        {focused === -1 && <NotFound />}
      </div>
    );
  }
}
export default Admin;
