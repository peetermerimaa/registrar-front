import React from "react";
import { observer } from "mobx-react";
import { menuParticipant } from "../../service/constants";
import UpcomingStore from "./UpcomingStore";
import Competition from "../competition/Competition";

@observer
class Upcoming extends React.Component {
  store = new UpcomingStore();

  componentDidMount() {
    this.store.init();
  }

  renderCompetitionList(competitions) {
    return competitions.map((competition, index) => (
      <Competition
        key={`${index}-${competition.name}`}
        type={Competition.UPCOMING}
        item={competition}
        store={this.store}
      />
    ));
  }

  render() {
    const index = this.props.index;

    return (
      <div className="innerContainer">
        <h1>{menuParticipant[index]}</h1>
        {this.store.competitions &&
          this.renderCompetitionList(this.store.competitions)}
      </div>
    );
  }
}
export default Upcoming;
