import Api from "../../service/Api";
import { observable, action } from "mobx";

class UpcomingStore {
  @observable competitions;
  @observable isRegistrationStarted;
  @observable accordionsMap = new Map();

  async init() {
    this.setCompetitions(await Api.getCompetitions());
    this.setAccordionsClosed();
  }

  @action
  setCompetitions(competitions) {
    this.competitions = competitions;
  }

  @action
  setIsRegistrationStarted(isRegistrationStarted) {
    this.isRegistrationStarted = isRegistrationStarted;
  }

  @action
  setAccordionOpened(id) {
    this.competitions.forEach(item =>
      this.accordionsMap.set(item.id, id === item.id)
    );
  }

  @action
  setAccordionsClosed() {
    this.competitions.forEach(item => this.accordionsMap.set(item.id, false));
  }

  isAccordionActive(id) {
    return !!this.accordionsMap.get(id);
  }
}

export default UpcomingStore;
