import Api from "../../service/Api";
import { observable, action } from "mobx";
import UpcomingStore from "./UpcomingStore";

class RecentStore extends UpcomingStore {
  @observable raceResults;

  async getRaceResults(idCompetition) {
    this.setRaceResults(await Api.getRaceResults(idCompetition));
  }

  @action
  setRaceResults(raceResults) {
    this.raceResults = raceResults;
  }

  @action
  clear() {
    this.setRaceResults(undefined);
  }
}

export default RecentStore;
