import React from "react";
import { inject, observer } from "mobx-react";
import Avaleht from "./Avaleht";
import Upcoming from "./Upcoming";
import Recent from "./Recent";
import NavigationStore from "../navigation/NavigationStore";
import NotFound from "../container/NotFound";

@inject("routing", "navigation")
@observer
class Participant extends React.Component {
  render() {
    const { routing, navigation } = this.props;
    const focused = navigation.focused(
      routing.location,
      NavigationStore.PARTICIPANT
    );

    return (
      <div className="box">
        {focused === 0 && <Avaleht index={0} />}
        {focused === 1 && <Upcoming index={1} />}
        {focused === 2 && <Recent index={2} />}
        {focused === -1 && <NotFound />}
      </div>
    );
  }
}
export default Participant;
