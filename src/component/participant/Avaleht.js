import React from "react";
import { menuParticipant } from "../../service/constants";

class Avaleht extends React.Component {
  render() {
    const { index } = this.props;
    return (
      <div className="innerContainer">
        <h1>{menuParticipant[index]}</h1>
        <p>
          15. veebruaril 2019 tutvustati uut rahvaspordivõitslustele
          registreerimise platvormi. <br />
          See oli Vali IT kursuse lõputööna valminud AjaLeht veebileht, mille
          abil saavad väiksemate võistluste korraldajad registreerida
          osavõtjaid, fikseerida nende lõpuaegu ja kiirelt kuvada tulemusi.
        </p>
      </div>
    );
  }
}
export default Avaleht;
